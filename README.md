# prova_sicredi_api


Projeto criado para processo seletivo Stefanini.

# Descriçao do projeto:

Desenvolver cenários de teste para consulta de CEP no site "https://viacep.com.br/"

# Tecnologias

* Java 8
* Maven
* Rest Assured 4.3.0
* Cucumber 4.4.0

# Executar testes
```bash
`mvn test`
```

# Notas

Para testes e validações da API, foi utilizado o Rest-assured.


Para desenvolver os testes e as validações com padrão BDD, foi utilizado o Cucumber.




